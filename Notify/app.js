import express from 'express';
import path from 'path';
import dotenv from 'dotenv'
dotenv.config()
const __dirname = path.resolve();
const app = express();
const PORT = process.env.PORT || 3000
import { Server } from "socket.io";
let x =app.listen(PORT, '0.0.0.0', () => {
    console.log(`Server has been started on port ${PORT}`)
})
const io = new Server(x, {});
io.on('connection', function(socket) {
    console.log('A user connected');
    socket.on('disconnect', function () {
       console.log('A user disconnected');
    });
 });
app.use(express.urlencoded({ extended: false }));
app.use(express.json())
app.use('/socket', function(req, res){
    res.sendFile(__dirname + '/index.html')
})