import express from 'express';
import dotenv from 'dotenv'
import routes from './src/routes/route.js';
import fs from 'fs';
dotenv.config()
const app = express();
const PORT = process.env.PORT || 3000
let x =app.listen(PORT, '0.0.0.0', () => {
    console.log(`Server has been started on port ${PORT}`)
})
app.use(express.urlencoded({ extended: false }));
app.use(express.json())
app.use('/api/v1', routes);
