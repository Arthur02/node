import authorize from './subroutes/authorization.js';

class Routes {
    constructor(app) {
        app.use('/api/v1', authorize);
    }
}

export default Routes