import express from 'express';
import dotenv from 'dotenv'
import Routes from './src/routes/route.js';
import swaggerUi from 'swagger-ui-express'
import fs from 'fs';
import winston from 'winston';
import 'winston-daily-rotate-file';

let swaggerFile = JSON.parse(fs.readFileSync('swagger-output.json', 'utf-8'))
dotenv.config()
const app = express();
const PORT = process.env.PORT || 3000
let x =app.listen(PORT, '0.0.0.0', () => {
    console.log(`Server has been started on port ${PORT}`)
})
var transport = new (winston.transports.DailyRotateFile)({
  filename: './logs/application-%DATE%.log',
  datePattern: 'YYYY-MM-DD-HH',
  zippedArchive: true,
  maxSize: '20m',
  maxFiles: '3d'
});
transport.on('rotate', function(oldFilename, newFilename) {

});
var logger = winston.createLogger({
  transports: [
    transport
  ]
});
logger.info('testLog!');
app.use(express.urlencoded({ extended: false }));
app.use(express.json())
app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerFile))
new Routes(app)
