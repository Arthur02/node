import Helper from './../helper/helper.js';
import User from './../models/user.js';
import bcrypt from 'bcrypt';
class UserController {
    constructor() {
    }

    async login(req, res, next) {
        /* 	#swagger.tags = ['User']
            #swagger.description = 'Endpoint to sign in a specific user' */
        /*	#swagger.parameters['data'] = {
                in: 'body',
                description: 'User credentials.',
                required: true,
                schema: { $ref: "#/definitions/UserLogin" }
        } */

        const { email, password } = req.body;
        if (!email || !password) {
            return res.status(403).send({
                error: 'Request missing email or password param'
            });
        }
        try {
            let user = await User.authenticate(email, password)
            const token = Helper.generateAccessToken({ email: user.email });
            res.status(200).send({
                token,
                id       :   user.id,
                role     :   user.role,
                name     :   user.name,
                email    :   user.email,
                username :   user.username
            });
        } catch (err) {
            return res.status(403).send({
                error: 'invalid email or password',
            });
        }
    }

    async register (req, res, next) {
        /* 	#swagger.tags = ['User']
            #swagger.description = 'Endpoint to sign up a specific user' */
        /*	#swagger.parameters['data'] = {
                in: 'body',
                description: 'User credentials.',
                required: true,
                schema: { $ref: "#/definitions/UserRegister" }
        } */
        const { email, password, name, username } = req.body 
        if(!email || !password || !name || !username){
            return res.status(403).send({
                error: 'Please fill all the inputs'
            });
        }
        try {
            let user = await User.register(email, password, name, username)
            return res.status(200).send(user);
        } catch (err) {
            const errObj = {};
            err.errors.map( er => {
                errObj[er.path] = er.message;
            })
            return res.status(403).send({
                'error': errObj
            });
        }
    }


    logout() {

    }
}
export default new UserController()